var express = require('express');
var router  = express.Router();

router.get('/', function(req, res){
   res.render('imsg', {title : "Message engine"});
});

module.exports = router;